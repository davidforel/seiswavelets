# seiswavelets

## A web-based seismic wavelet creator.

Design:
- A designer section that includes the choice of the wavelet, the phase, and the output length.
- QC displays of the time series, frequency spectrum, and phase.
- An output button to export the designed wavelet.
### Wavelet Creator Mockup
![wavelet creator mockup!](WaveletDesignerMockup-05.png "Wavelet Creator Mockup")